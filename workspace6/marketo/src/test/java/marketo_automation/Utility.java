package marketo_automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public  class Utility {

	
	static String synthio_url()
	{
		return "https://qa.synthio.com";
		}
	 public static  void clickElement(String attribute, WebDriver driver, int type, int action, String string) {
		// Click
		if (type == 1 && action == 1) {
			driver.findElement(By.className(attribute)).click();
		}
		if (type == 2 && action == 1) {
			driver.findElement(By.id(attribute)).click();
		}
		if (type == 3 && action == 1) {
			driver.findElement(By.xpath(attribute)).click();
		}
		// Send key
		if (type == 1 && action == 2) {
			driver.findElement(By.className(attribute)).sendKeys(string);
		}
		if (type == 2 && action == 2) {
			driver.findElement(By.id(attribute)).sendKeys(string);
		}
		if (type == 3 && action == 2) {
			// driver.findElement(By.xpath(attribute)).clear();
			driver.findElement(By.xpath(attribute)).sendKeys(string);
		}

	}
	 
	
	 
	
}
