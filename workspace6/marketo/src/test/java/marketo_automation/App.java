package marketo_automation;

import com.eclipsesource.json.JsonObject;

public class App 
{
    public static void main( String[] args )
    {
    	//Create an instance of Auth so that we can authenticate with our Marketo instance
        //Change the credentials here to your own
        Auth auth = new Auth("362d4c15-0a34-444c-bedd-a4e215654f87", "aHA3z5TVQrl9gIvRmW9LUk7lqsdGlMyE", "https://412-CBA-821.mktorest.com");
        //Create and parameterize an instance of Leads
        Leads getLeads = new Leads(auth)
        						.setFilterType("email")
        						.addFilterValue("testlead@marketo.com");
        //get the inner results array of the response
        JsonObject leadsResult = getLeads.getData();
        System.out.println(leadsResult);
        
    }
}