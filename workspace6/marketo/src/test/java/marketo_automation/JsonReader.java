package marketo_automation;

import java.io.IOException;
import java.util.Collections;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.ParseException;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;

public class JsonReader {

	
	static String data = "";

	public static String getData(JSONObject jobj, String tag) throws JSONException {
		if (jobj.has(tag)) {
			data = jobj.get(tag).toString();
		} else {
			data = "";
		}
		return data;
	}

	public static String getConnection(String query) throws ParseException, IOException {
		RestClient restClient = RestClient.builder(

				new HttpHost("172.31.91.154",5601, "http")).setMaxRetryTimeoutMillis(150000).build();
		HttpEntity entity1 = new NStringEntity(query, ContentType.APPLICATION_JSON);

		Response response = restClient.performRequest("GET", "contact_n18/businessContact/_search",
				Collections.singletonMap("pretty", "true"), entity1);
		return EntityUtils.toString(response.getEntity());
	}

	public static String getConnectionAppend(String query) throws ParseException, IOException {
		RestClient restClient = RestClient.builder(

				new HttpHost("172.31.91.154", 5601, "http")).setMaxRetryTimeoutMillis(150000).build();
		HttpEntity entity1 = new NStringEntity(query, ContentType.APPLICATION_JSON);

		Response response = restClient.performRequest("GET", "hq_n18/businessCompany/_search",
				Collections.singletonMap("pretty", "true"), entity1);
		return EntityUtils.toString(response.getEntity());
	}

}
