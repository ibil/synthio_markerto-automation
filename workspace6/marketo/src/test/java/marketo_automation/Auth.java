package marketo_automation;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import com.eclipsesource.json.JsonObject;

public class Auth {
	protected String marketoInstance; //Instance Host obtained from Admin -> Web Service
	private String clientId; //clientId obtained from Admin -> Launchpoint -> View Details for selected service
	private String clientSecret; //clientSecret obtained from Admin -> Launchpoint -> View Details for selected service
	private String idEndpoint; //idEndpoint constructed to authenticate with service when constructor is used
	private String token; //token is stored for reuse until expiration
	private Long expiry; //used to store time of expiration
	
	//Creates an instance of Auth which is used to Authenticate with a particular service on a particular instance
	public Auth(String id, String secret, String instanceUrl) {
		this.clientId = id;
		this.clientSecret = secret;
		this.marketoInstance = instanceUrl;
		this.idEndpoint = marketoInstance + "/identity/oauth/token?grant_type=client_credentials&client_id=" + clientId + "&client_secret=" + clientSecret;
	}
	//The only public method of Auth, used to check if the current value of Token is valid, and then to retrieve a new one if it is not
	public String getToken(){
		long now  = System.currentTimeMillis();
		if (expiry == null || expiry < now){
			System.out.println("Token is empty or expired. Trying new authentication");
			JsonObject jo = getData();
			System.out.println("Got Authentication Response: " + jo);
			this.token = jo.get("access_token").asString();
                        //expires_in is reported as seconds, set expiry to system time in ms + expires * 1000
			this.expiry = System.currentTimeMillis() + (jo.get("expires_in").asLong() * 1000);
		}
		return this.token;
	}
	//Executes the authentication request
	private JsonObject getData(){
		JsonObject jsonObject = null;
		try {
			URL url = new URL(idEndpoint);
			HttpsURLConnection urlConn = (HttpsURLConnection) url.openConnection();
			urlConn.setRequestMethod("GET");
            urlConn.setRequestProperty("accept", "application/json");
            System.out.println("Trying to authenticate with " + idEndpoint);
            int responseCode = urlConn.getResponseCode();
            if (responseCode == 200) {
                InputStream inStream = urlConn.getInputStream();
                Reader reader = new InputStreamReader(inStream);
                jsonObject = JsonObject.readFrom(reader);
            }else {
                throw new IOException("Status: " + responseCode);
            }
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}catch (IOException e) {
            e.printStackTrace();
        }
		return jsonObject;
	}
}