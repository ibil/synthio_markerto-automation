package marketo_automation;


import java.awt.AWTException;	
import java.awt.Robot;	
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import java.util.Random; 

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class case1_origination {
	//Case 17 -IBIL.ML
	
	WebDriver driver;
	String name;
	int testId = 1;
	int num;
	String s;
	Boolean status;
	
	private Logger logger = Logger.getLogger(getClass().getName());
	
	private String path = "lib/Test_data.xlsx";
	private String sheetName = "AbmComapnyFields";
//	List<String> data = ReadData.getData(path, sheetName);
	String downloadFilepath = System.getProperty("user.dir") + "\\lib\\temp\\";
	String baseUrl ;
	//String downloadFilepath = "C:\\Users\\Administrator\\Downloads\\";
	//Checking Url Availability
	String program = "Default";
	
	String list = "IBIL TEST LIST-1";
	@BeforeTest
	public void checkAvailability() {
		String value = "Naveen.nair@acolade.com";
		String type = "Email";
		
	

		System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.default_directory", downloadFilepath);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);
		//options.addArguments("--headless");
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(cap);
		driver.manage().window().maximize();
		 baseUrl =Utility.synthio_url();
		driver.get(baseUrl);
		
		
		
		
		
	}
	@Test(priority = 1)
	public void login() throws Exception {
	
			logger.info("Login Started");
			Login.get(driver);
			logger.info("Login Finished");
			Thread.sleep(1000);
			
		
	}	

	@Test(priority = 2)
	public void contactSearch() throws Exception{
		
		driver.get("https://qa.synthio.com/ui/search");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"advancedButton\"]/a[2]")).click();
		//entering the name for search 
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@ng-model=\"options.fullName\"]")).sendKeys("aswin ");
		
		Thread.sleep(2000);
		//enter the name of the company 
		driver.findElement(By.xpath("(//input[@ng-model=\"options.company\"])[2]")).sendKeys("gsk");
		
		
		
		
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"searchButton\"]/button")).click();
		Thread.sleep(2000);
			
	
		
		
	}
	@Test(priority = 3)
	public void createcsv() throws Exception{
		
			logger.info("createcsv");
			System.out.print("enter here");
			
					Random rand = new Random(); 
			num=rand.nextInt(1000);
	s=Integer.toString(num);
			 name="createscv"+s;
		
			System.out.print(name);
			Thread.sleep(6000);
			
			//create CSV
		driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/ng-view/div[3]/div[1]/div[2]/div[2]/div/button")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//ul/li/a[text()=\"Create CSV File\"]")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//div/input[@ng-model=\"options.jobTitle\"]")).sendKeys(name);
			Thread.sleep(1000);
			driver.findElement(By.xpath("//div/input[@value=\"Create File\"]")).click();
			Thread.sleep(1000);
			driver.switchTo().alert().accept();
			
			
			

			
		
	}
	
	@Test(priority=4)
	public void orgin_export() throws InterruptedException {
		logger.info("origination export");
		Boolean status;
		
		driver.get("https://qa.synthio.com/ui/assurance/originations");
		
		Thread.sleep(3000);
		int i=0;
		while(i==0) {
			
			driver.findElement(By.id("fileName")).sendKeys(name);
			Thread.sleep(1000);
			driver.findElement(By.id("originationFilterButton")).click();
			Thread.sleep(1000);
			driver.findElement(By.id("dashboardActivity-options")).click();
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
			status=driver.findElement(By.xpath("//li/a[text()=\"Export to Marketo\"]")).isDisplayed();
			 
			if (status == true) {
				i = 1;
				break;
			}
			
				else 
					i=0;
			driver.navigate().refresh();
			Thread.sleep(6000);
			
		}
		
		driver.findElement(By.xpath("//li/a[text()=\"Export to Marketo\"]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"marketoPublishOrig\"]")).click();
		
		Thread.sleep(5000);		
		driver.navigate().refresh();
		
	}
	
	@Test(priority = 5)
	public void downloadCsv() throws Exception{
		
			logger.info("Download CSV Started");
			System.out.println("downloading started");
			boolean status;
			Thread.sleep(5000);
			driver.findElement(By.id("fileName")).sendKeys(name);
			System.out.println("name entered");
			Thread.sleep(1000);
			driver.findElement(By.id("originationFilterButton")).click();
			System.out.println("apply filter click");
			Thread.sleep(5000);
			driver.findElement(By.id("dashboardActivity-options")).click();
			System.out.println("option button click");
			Thread.sleep(8000);
			Thread.sleep(5000);	
			
			Thread.sleep(5000);	
			status = driver.findElement(By.xpath("//*[@id=\"page-container\"]/table/tbody/tr[1]/td[9]/div/div/ul/li[2]")).isDisplayed();
			
			if (status == false) {
			Thread.sleep(5000);	
			driver.findElement(By.xpath("//*[@id=\"page-container\"]/table/tbody/tr[1]/td[9]/div/div/ul/li[2]")).click();
			Thread.sleep(5000);
			
			Robot rb= new Robot();

	    	rb.keyPress(KeyEvent.VK_ENTER);

			} 
			
			else {
							
			Thread.sleep(5000);
			driver.navigate().refresh();
			Thread.sleep(5000);
			driver.findElement(By.id("dashboardActivity-options")).click();
			Thread.sleep(5000);	
			driver.findElement(By.xpath("//*[@id=\"page-container\"]/table/tbody/tr[1]/td[9]/div/div/ul/li[2]")).click();
			Thread.sleep(5000);
			Robot rb= new Robot();
			rb.keyPress(KeyEvent.VK_ENTER);
			System.out.println("Download is not===== working");
			}
			
			

	}
	
		@Test(priority = 6)
		public void dataValidation()throws Exception {
		
	
			System.out.println("Data compare started");
			logger.info("Data Validation Started");
			
			logger.info("Invoking Marketo API");
			
			
			
			
			logger.info("Data Validation Finished");
			
			ReadCSV.readDataLineByLine(downloadFilepath+list+"_results.csv");
			
			
		}			

	
//	@AfterTest
//	public void close() {
//	
//	driver.quit();
//	}
	
	
	
	
	
	
	


}
