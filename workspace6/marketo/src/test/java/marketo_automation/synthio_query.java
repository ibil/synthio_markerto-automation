package marketo_automation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.ParseException;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class synthio_query {
	
	
	public static void runquery(String email) throws ParseException, IOException {
		
		
		String query="Query: {\"track_total_hits\":true,\"from\":0,\"query\":{\"bool\":{\"filter\":[{\"terms\":{\"emailAddressCleansed\":[\"moirangthemaraveinfosyscom\"]}}]}},\"size\":105,\"track_scores\":true}";
		
		String json = JsonReader.getConnection(query);

		JSONObject object = new JSONObject(json);
		JSONObject getObject = object.getJSONObject("hits");
		JSONArray getArrayhits = getObject.getJSONArray("hits");
		for (int i = 0; i < getArrayhits.length(); i++) {
			JSONObject jsonmain = getArrayhits.getJSONObject(i);
			JSONObject getObjectsource = jsonmain.getJSONObject("_source");

			String ContactKey = JsonReader.getData(jsonmain, "_id");
			String ContactSearchUrl = JsonReader.getData(getObjectsource, "searchUrl");
			String ContactFirstName = JsonReader.getData(getObjectsource, "firstName");
			String ContactLastName = JsonReader.getData(getObjectsource, "lastName");

			String ContactPosition = JsonReader.getData(getObjectsource, "currentCompanyPosition");
			String ContactDepartment1 = JsonReader.getData(getObjectsource, "titleDepartment1");
			String ContactLevel = JsonReader.getData(getObjectsource, "titleLevel");
			String ContactDepartmentFunction = JsonReader.getData(getObjectsource, "titleDepartmentFunction");
			String ContactDecisionMaker = JsonReader.getData(getObjectsource, "decisionMaker");
			String ContactHQCompanyName = JsonReader.getData(getObjectsource, "currentCompanyName");
			String ContactCity = JsonReader.getData(getObjectsource, "city");
			String ContactRegionCode = JsonReader.getData(getObjectsource, "region");
			String ContactRegionName = JsonReader.getData(getObjectsource, "regionName");
			String ContactCountry = JsonReader.getData(getObjectsource, "countryName");
			String ContactLatitude = JsonReader.getData(getObjectsource, "latitude");
			String ContactLongitude = JsonReader.getData(getObjectsource, "longitude");
			String ContactTimeZone = JsonReader.getData(getObjectsource, "timeZone");
			String ContactEmailAddress = JsonReader.getData(getObjectsource, "emailAddress");

			String ContactNormalizedTitle = "", ContactPhone = "", HQCompanyID = "", HQCompanyName = "",
					HQCompanyStreet1 = "", HQCompanyCity = "", HQCompanyRegionCode = "", HQCompanyPostalCode = "",
					HQCompanyIndustry = "", HQCompanyType = "", HQCompanySize = "", HQCompanyWebsite = "",
					HQCompanyRevenue = "", HQCompanyExactRevenue = "";

			JSONArray getObjectexperence = getObjectsource.getJSONArray("experience");

			// System.out.println(ContactLastName);
			JSONObject jsonexp = getObjectexperence.getJSONObject(0);
			ContactNormalizedTitle = JsonReader.getData(jsonexp, "normalizedTitle");
			if (getObjectsource.has("currentCompany")) {

				JSONObject jsoncurrent = getObjectsource.getJSONObject("currentCompany");

				if (jsonexp.has("locationSiteRecord")) {
					JSONObject jsonlocalsite = jsonexp.getJSONObject("locationSiteRecord");

					ContactPhone = JsonReader.getData(jsonlocalsite, "phoneNumber");

					HQCompanyID = JsonReader.getData(jsoncurrent, "id");
					HQCompanyName = JsonReader.getData(jsoncurrent, "Name");
					HQCompanyStreet1 = JsonReader.getData(jsoncurrent, "street1");
					HQCompanyCity = JsonReader.getData(jsoncurrent, "City");

					HQCompanyRegionCode = JsonReader.getData(jsoncurrent, "Region");
					HQCompanyPostalCode = JsonReader.getData(jsoncurrent, "postal");
					HQCompanyIndustry = JsonReader.getData(jsoncurrent, "Industry");
					HQCompanyType = JsonReader.getData(jsoncurrent, "Type");
					HQCompanySize = JsonReader.getData(jsoncurrent, "Size");

					HQCompanyWebsite = JsonReader.getData(jsoncurrent, "Website");
					HQCompanyRevenue = JsonReader.getData(jsoncurrent, "revenue");
					HQCompanyExactRevenue = JsonReader.getData(jsoncurrent, "exactRevenue");
				}

			}

		
	
		
}

		

		
	}
	

}



//Query: {"track_total_hits":true,"from":0,"query":{"bool":{"filter":[{"terms":{"emailAddressCleansed":["moirangthemaraveinfosyscom"]}}]}},"size":105,"track_scores":true}	