package marketo_automation;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;

import com.eclipsesource.json.JsonObject;
 
public class Leads {
	private StringBuilder endpoint;
	private Auth auth;
	public String filterType;
	public ArrayList filterValues = new ArrayList();
	public Integer batchSize;
	public String nextPageToken;
	public ArrayList fields = new ArrayList();
	
	public Leads(Auth a) {
		this.auth = a;
		this.endpoint = new StringBuilder(this.auth.marketoInstance + "/rest/v1/leads.json");
	}
	public Leads setFilterType(String filterType) {
		this.filterType = filterType;
		return this;
	}
	public Leads setFilterValues(ArrayList filterValues) {
		this.filterValues = filterValues;
		return this;
	}
	public Leads addFilterValue(String filterVal) {
		this.filterValues.add(filterVal);
		return this;
	}
	public Leads setBatchSize(Integer batchSize) {
		this.batchSize = batchSize;
		return this;
	}
	public Leads setNextPageToken(String nextPageToken) {
		this.nextPageToken = nextPageToken;
		return this;
	}
	public Leads setFields(ArrayList fields) {
		this.fields = fields;
		return this;
	}
 
	public JsonObject getData() {
        JsonObject result = null;
        try {
        	endpoint.append("?access_token=" + auth.getToken() + "&filterType=" + filterType + "&filterValues=" + csvString(filterValues));
        	if (batchSize != null && batchSize > 0 && batchSize <= 300){
            	endpoint.append("&batchSize=" + batchSize);
            }
            if (nextPageToken != null){
            	endpoint.append("&nextPageToken=" + nextPageToken);
            }
            if (fields != null){
            	endpoint.append("&fields=" + csvString(fields));
            }
            URL url = new URL(endpoint.toString());
            HttpsURLConnection urlConn = (HttpsURLConnection) url.openConnection();
            urlConn.setRequestMethod("GET");
            urlConn.setRequestProperty("accept", "text/json");
            InputStream inStream = urlConn.getInputStream();
            Reader reader = new InputStreamReader(inStream);
            result = JsonObject.readFrom(reader);
            
            
        } catch (MalformedURLException e) {
            System.out.println("URL not valid.");
        } catch (IOException e) {
            System.out.println("IOException: " + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }
	private String csvString(ArrayList fields) {
		StringBuilder fieldCsv = new StringBuilder();
    	for (int i = 0; i < fields.size(); i++){
    		fieldCsv.append(fields.get(i));
    		if (i + 1 != fields.size()){
    			fieldCsv.append(",");
    		}
    	}
		return fieldCsv.toString();
	}
}