package marketo_automation;

import java.io.FileReader;
import java.util.List;

import org.testng.Assert;

import com.opencsv.CSVReader;

public class ReadCSV {
		// Java code to illustrate reading a
		// CSV file line by line
		static List<String[]> csv_list;

		 public static List<String[]> readDataLineByLine(String file) {
//			System.out.println(file);

			try {

				// Create an object of filereader
				// class with CSV file as a parameter.
				FileReader filereader = new FileReader(file);

				// create csvReader object passing
				// file reader as a parameter
				CSVReader csvReader = new CSVReader(filereader);
				String[] nextRecord;

				// we are going to read data line by line
				csvReader.readNext();

				// System.out.print(cell);
				csv_list = csvReader.readAll();
			validation.validationcode(csv_list);
				csvReader.close();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return csv_list;
		}

		static void check_validate(String record, String criteria) {

			record = record.replaceAll("[^\\p{ASCII}]", "");
			criteria = criteria.replaceAll("[^\\p{ASCII}]", "");
			if (!record.equals(null) || !criteria.equals(null)) {
				if (record.equalsIgnoreCase(criteria)) {

				} else {
					Assert.assertNotEquals(record, criteria, "Not Equal");
				}
			}

		}

		

}
