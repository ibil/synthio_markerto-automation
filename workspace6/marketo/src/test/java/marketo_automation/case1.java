package marketo_automation;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.eclipsesource.json.JsonObject;

public class case1 {
	//Case 17 -IBIL.ML
	
	WebDriver driver;
	String name;
	int testId = 1;
	private Logger logger = Logger.getLogger(getClass().getName());
	
	private String path = "lib/Test_data.xlsx";
	private String sheetName = "AbmComapnyFields";
//	List<String> data = ReadData.getData(path, sheetName);
	String downloadFilepath = System.getProperty("user.dir") + "\\lib\\temp\\";
	String baseUrl = Utility.synthio_url();
	//String downloadFilepath = "C:\\Users\\Administrator\\Downloads\\";
	//Checking Url Availability
	String program = "Default";
	
	String list = "IBIL TEST LIST-1";
	@BeforeTest
	public void checkAvailability() {
		String value = "Naveen.nair@acolade.com";
		String type = "Email";
		String[] email=new String[] {"hunter.eskew@synthiocorp.com"};


	
	
		
	

		System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.default_directory", downloadFilepath);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);
		//options.addArguments("--headless");
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(cap);
		driver.manage().window().maximize();
		String baseUrl =Utility.synthio_url();
		driver.get(baseUrl);
		
		// to edit lead data before performing marketo synthio append//
		for(int i =1;i<=4;i++)
		{
			switch(i) {
			case 1:
				String datalist1[]= {"Naveen,Nair,mdetolla@tableau.com, "};
				String cat[]= {"firstname, lastname, email,website"};
				editleads.getdetails(cat, datalist1);
				break;
				
			case 2:
				String datalist2[]= {"Julia,julia.burova@xerox.com,info"}; 		
				String cat2[]= {"firstname,email,company"};
				editleads.getdetails(cat2, datalist2);

				break;
			
				
				
				
			case 3:
				String datalist3[]= {"Rajendran,Perumal, "};
				String cat3[]= {"firstname, lastname,email"};
				editleads.getdetails(cat3, datalist3);
				break;
				
			
				
			case 4:
				String datalist4[]= {"Lalida,Chir,lalida.chirasheve@dccomics.com"};
				String cat4[]= {"firstname, lastname,email"};
				editleads.getdetails(cat4, datalist4);
				break;
				
		
				
			
				
				
				
				
				
	
				
				
				
			}
		}
		
		
		
		
	}
	@Test(priority = 1)
	public void login() throws Exception {
	
			logger.info("Login Started");
			Login.get(driver);
			logger.info("Login Finished");
			Thread.sleep(1000);
			
		
	}	

	@Test(priority = 2)
	public void contactSearch() throws Exception{
		
		driver.get("https://qa.synthio.com/ui/append");
		Thread.sleep(6000);
		driver.findElement(By.id("importMarketo")).click();
		
		Thread.sleep(10000);
		
	
		
		//Programs
		driver.findElement(By.xpath("//div/span/span/span/span[@class=\"k-select\"]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id='mktoProgramDropdown_listbox']/li[4]")).click();

		//List
		//Select drplist = new Select( driver.findElement(By.id("mktoListDropdown_option_selected")));
		//drplist.selectByVisibleText("EU webhook static list");
		driver.findElement(By.xpath("//*[@id=\"modal_\"]/div/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div[2]/span[2]/span/span/span[1]")).click();
		Thread.sleep(3000);
		
		
		driver.findElement(By.xpath("//*[@id='mktoListDropdown_listbox']/li")).click();
		System.out.println("One");
		Thread.sleep(3000);
//		driver.findElement(By.xpath("//*[@id=\"mktoListDropdown_option_selected\"]")).click();
		System.out.println("Two...............................");
		Thread.sleep(3000);
//		driver.findElement(By.id("optionButton")).click();
		System.out.println("Three...............................");
	//	driver.findElement(By.xpath("//*[@id='regularContactAppend']/input")).click();
//		driver.findElement(By.id("processButton")).click();
		driver.findElement(By.xpath("//button[text()=\"Import List\"]")).click();
		System.out.println("Four...............................");
		Thread.sleep(3000);
		
		//Alert alert = driver.switchTo().alert();
		System.out.println("five...............................");
		//alert.accept();
		
		
		
	}
//	@Test(priority = 3)
//	public void downloadCsv() throws Exception{
//		
//			logger.info("Download CSV Started");
//			Boolean status;
//			Thread.sleep(5000);
//
//			Utility.clickElement("//*[@id=\"page-container\"]/div[1]/div[2]/div[1]/div[2]/div/input", driver, 3, 2,list);
//			Utility.clickElement("appendFilterButton", driver, 2, 1, "");
//
//			Thread.sleep(4000);
//			driver.findElement(By.id("dashboardActivity-options")).click();
//			Thread.sleep(4000);
//
//			int i = 0;
//			while (i == 0) {
//				JavascriptExecutor js = (JavascriptExecutor) driver;
//				js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
//				status = driver.findElement(By.xpath("(//a[contains(text(),'Download CSV')])[2]")).isDisplayed();
//
//				if (status == true) {
//					i = 1;
//					break;
//
//				} else
//					i = 0;
//				driver.navigate().refresh();
//				Thread.sleep(5000);
//				Utility.clickElement("//*[@id=\"page-container\"]/div[1]/div[2]/div[1]/div[2]/div/input", driver, 3, 2,
//						list);
//				Utility.clickElement("appendFilterButton", driver, 2, 1, "");
//
//				Thread.sleep(4000);
//				driver.findElement(By.id("dashboardActivity-options")).click();
//
//			}
//			Thread.sleep(3000);
//			JavascriptExecutor js = (JavascriptExecutor) driver;
//			js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
//			Thread.sleep(2000);
//			
//			//Export to Marketo 
//			driver.findElement(By.xpath("(//a[contains(text(),'Export to Marketo')])")).click();
//			Thread.sleep(4000);
//			driver.findElement(By.xpath("//div/button[text()=\"Publish\"]")).click();
//			Thread.sleep(10000);
//
//			
//		//driver.findElement(By.xpath("//*[@id=\"modal_\"]/div/div/div[2]/div[2]/div/div[2]/div[2]/div/div[2]/div/div[3]/button[1]")).click();
//			driver.findElement(By.id("dashboardActivity-options")).click();
//			
//			driver.findElement(By.xpath("(//a[contains(text(),'Download CSV')])[2]")).click();
//			logger.info("Export to Marketo  and Download CSV Finished");
//		
//	}
	
	//@Test(priority = 5)
	//public void dataValidation()throws Exception {
		
//		
//		
//			logger.info("Data Validation Started");
//			
//			logger.info("Invoking Marketo API");
//			
//			
//			
//			
//			logger.info("Data Validation Finished");
//			
//			ReadCSV.readDataLineByLine(downloadFilepath+list+"_results.csv");
//			
//			
			
			
		
	
	
	


//}
}
